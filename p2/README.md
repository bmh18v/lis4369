> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Bryan Humphries

### Project 2 Requirements:

*Three parts:*

1. Backward-engineer supplied lis4369_p2_requirements.txt file
2. Screenshots of at least two plots (with *my* name in plot titles) from lis4369_p2.R file 
3. Run coded program in RStudio and check for errors

#### README.md file should include the following items:

* Screenshot of backward-engineered lis_p2_requirements.txt file
* Screenshots of at least two plots from the coded requirements.txt file with *my* name on the titles
* Screenshot of soads.R file code and output
* Screenshots of plots from soads.R file output

#### Assignment Screenshots:

#### *4-Panel* Requirements.txt Code and Output

##### **Requirements.txt Screenshot 1**
![Requirements.txt File Code Output 1](img/requirements_p2txt_code_and_output.png "Requirements.txt Code and Output 1")

##### **Requirements.txt Screenshot 2**
![Requirements.txt File Code Output 2](img/requirements_p2txt_codep2.png "Requirements.txt Code and Output 2")

#### Plots from Requirements.txt file

| Displacement v. MPG Plot | Weight v. MPG Plot
| :-|:-|:-
![Displacement vs. MPG Plot](img/plot_disp_and_mpg_1.png "Displacement v. MPG") | ![Weight v. MPG](img/plot_disp_and_mpg_2.png "Weight v. MPG") 



#### Soads.R *4-Panel* Screenshot

![Soads.R Program Screenshot](img/soads_r_screenshot.png "Soads.R Screenshot")

#### Soads.R Panel Screenshots

| Soads Plot 1 | Soads Plot 2
| :-|:-|:-
![Soads Plot 1](img/pic1_soads.png "Soads Plot 1") | ![Soads Plot 2](img/pic2_soads.png "Soads Plot 2")






