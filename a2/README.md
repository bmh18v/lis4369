> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Bryan Humphries

### Assignment 2 Requirements:

*Three parts:*

1. Assignment Requirements
2. Screenshots
3. A2.ipynb file linked

#### README.md file should include the following items:

* Screenshots of application running
* link to A2 .ipynb file: [a2_payroll.ipynb](a2_payroll.ipynb "A2 Jupyter Notebook")


#### Assignment Screenshots:

| Screenshot of A2 Payroll with no OT: | Screenshot of A2 Payroll with OT:
| :-|:-|:-
![A2 Payroll Screenshot with no OT](img/payroll_no_ot.png "A2 No OT Screenshot") | ![A2 Payroll Screenshot with OT](img/payroll_ot.png "A2 OT Screenshot")

| Screenshot 1 of Jupyter Notebook: | Screenshot 2 of Jupyter Notebook:
| :-|:-|:-
![Jupyter Notebook Screenshot 1](img/a2_payroll1.png "A2 JN Pic 1") | ![Jupyter Notebook Screenshot 2](img/a2_payroll2.png "A2 JN Pic 2")

| Skillset 1: | Skillset 2: | Skillset 3:
| :-|:-|:-
![Skill Set 1](img/sqft_to_acres.png "Acres to SQFT") | ![Skill Set 2](img/MPG.png "Miles Per Gallon") | ![Skill Set 3](img/IT_ICT.png "Student Percentage")