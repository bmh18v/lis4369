> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Bryan Humphries

### Project 1 Requirements:

*Three parts:*

1. Data Analysis 1 Running in Terminal
2. Data Analysis 1 Running in Jupyter Notebook 
3. Provide Screenshots of Skillsets 7, 8, and 9.

#### README.md file should include the following items:

* Screenshot of *Data Analysis 1* application running
* Link to P1 Data Analysis 1 .ipynb file: [p1_data_analysis.ipynb](p1_data_analysis.ipynb "A3 Jupyter Notebook")
* Provide Screenshots of completed Python Skill Sets 7, 8, and 9.
* Screenshot of *Data Analysis* running in Jupyter Notebook

#### Assignment Screenshots:

#### Data Analysis in Terminal

| Data Analysis 1 in Terminal | Graph from Data Analysis 1
| :-|:-|:-
![Data Analysis 1 Terminal](img/data_analysis_1_commandline.png "Data Analysis 1") | ![Screenshot of Data Analysis 1 Graph](img/p1_graph_ss.png "P1 Line Graph")

#### Data Analysis 1 in Jupyter Notbook

| Data Analysis Code Part 1 | Data Analysis Part 2 | Code Output
| :-|:-|:-
![Jupyter Notebook Code Part 1](img/jupyter_notebook_p1_s1.png "JN Code Part 1") | ![Jupyter Notebook Code Part 2](img/jupyter_notebook_p1_s2.png "JN Code Part 2") | ![Jupyter Notebook Output](img/jupyter_notebook_p1_s3.png "JN Code Output")



#### Skill Sets 7-9

| Skillset 7: Using Lists | Skillset 8: Using Tuples | 
| :-|:-|:-
![Skill Set 7](img/ss7_scrennshot.png "Using Lists") | ![Skill Set 8](img/ss8_screenshot.png "Using Tuples") |

| Skillset 9: Using Sets Part 1 | Skillset 9: Using Sets Part 2
| :-|:-|:-
![SS9 Part 1](img/ssn_pic1.png "Part 1 Using Sets") | ![SS9 Part 2](img/ss9_pic2.png "Part 2 Using Sets")



