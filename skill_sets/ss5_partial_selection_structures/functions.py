def get_requirements():
    print("Developer: Bryan Humphries")
    print("Python Selection Structures")
    print("\nProgram Requirements:")
    print("1. Use Python selection structure.")
    print("2. Prompt the user for two numbers, and a suitable operator.")
    print("3. Test for correct numeric operator.")
    print("4. Replicate display below.\n")

def print_selection_structures():
    print("Python Calculator")
    # calculator
    num1 = float(input("Enter num1: "))
    num2 = float(input("Enter num2: "))
    print("\nSuitable Operators: +,-,*,/,// (integer division), % (modulo operator), **(power)")
    op = input("Enter operator:")

    if op == "+":
        print(num1 + num2)
        # or...
    
    elif op == "-":
        print(num1 - num2)
    
    elif op == "*":
        print(num1 * num2)
    
    elif op == "/":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 / num2)

    elif op == "//":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 // num2)

    elif op == "%":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 % num2)

    elif op == "**":
        print(num1 ** num2)
        # or..
        print(pow(num1,num2))
    else:
        print("Incorrect operator!")    
     