> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Bryan Humphries

### Assignment 3 Requirements:

*Three parts:*

1. Painting Estimator Running in Terminal
2. Painting Estimator Running in Jupyter Notebook
3. Provide Screenshots of Skillsets 4, 5, and 6

#### README.md file should include the following items:

* Screenshot of *Painting Estimator* application running
* Link to A3 Painting Estimator.ipynb file: [paint_estimator.ipynb](paint_estimator.ipynb "A3 Jupyter Notebook")
* Provide Screenshots of completed Python Skill Sets 4, 5, and 6.
* Screenshot of *Painting Estimator* running in Jupyter Notebook

#### Assignment Screenshots:

#### Painting Estimator in Terminal

| First Loop in Paint Estimator | Second Loop "Estimate Another Paint Job?"
| :-|:-|:-
![First Loop in PE](img/vs_part1.png "First Loop of Paint Estimator") | ![Second Loop of PE](img/vs_part2.png "Second Loop of Paint Estimator")

#### Painting Estimator in Jupyter Notbook

| Jupyter Notebook Code Part 1 | Jupyter Notebook Code Part 2 | Jupyter Notebook Code Output
| :-|:-|:-
![Jupyter Notebook Code Part 1](img/jn_codep1.png "JN Code Part 1") | ![Jupyter Notebook Code Part 2](img/jn_codep2.png "JN Code Part 2") | ![Jupyter Notebook Output](img/jn_output.png "JN Coce Output")


#### Skill Sets 4-6

| Skillset 4: Calorie Calculator | Skillset 5: Valid Operator | Skillset 5: Invalid Operator
| :-|:-|:-
![Skill Set 4](img/ss4_screenshot.png "Calorie Calculator") | ![Skill Set 5](img/ss5_valid_op.png "Valid Operator") | ![Skill Set 5](img/ss5_invalid_op.png "Invalid Operator")

| Skillset 6: Loops 1-4 | Skillset 6: Loops 5-9
| :-|:-|:-
![Loop Program 1-4](img/pythonloopingpart1.png "Part 1 Python Looping") | ![Loop Program 5-9](img/pythonloopingpart2.png "Part 2 Python Looping")



