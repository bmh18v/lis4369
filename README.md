> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Bryan Humphries

### LIS 4369 Requirements:

### Assignments:

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install Python
	- Install R
	- Install R Studio
	- Install Visual Studio Code
	- Create *a1_tip_calculator* application
	- Create *a1_tip_calculator* Jupyter Notebook
	- Provide screenshots of installations
	- Create bitbucket repo
	- Complete Bitbucket tutorial (bitbucketstationlocations)
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create Payroll app using "Separation of Concerns" design principles
	- Provide screenshots of completed Payroll Calculator
	- Provide screenshots of completed all 3 Python Skill Sets
	- Provide Screenshots of completed Jupyter Notebook 

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Provide screenshots of completed Python Skill Sets 4,5, and 6
	- Painting Estimator Screenshots in Terminal
	- Painting Estimator Screenshots in Jupyter Notebook

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Link to Jupyter Notebook Data Analysis 2 File
	- Screenshots of *Data Analysis 2* program running
	- Data Analysis 2 Code and Output in Jupyter Notebook
	- Provide screenshots of  Python Skill Sets 10, 11, and 12

5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Provide screenshots of learn_to_use.R and lis_4369.R code outputs
	- Provide screenshots of at least two plots from the tutorial and assignment file
	- Screenshots of Python Skillsets 13, 14, and 15

### Projects:

 1. [P1 README.md](p1/README.md "My P1 README.MD file")
	- Data Analysis Screenshots in Command Prompt
	- Data Analysis Screenshots in Jupyter Notebook
	- Provide Screenshots of skillsets 7, 8, and 9

2. [P2 README.md](p2/README.md "My P2 README.md file")
	- Two *4-panel* screenshots executing lis4369_p2.R code
	- Include at least two plots with name in plot titles
	- Screenshots from soads.R file output


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
