
# LIS 4369 - Extensible Enterprise Solutions

## Bryan Humphries

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Butbucket
2. Development Installations
3. Questions

4. Bitbucket repo links:
	a) this assignment and
	b) the completed tutorial

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* Link to A1 .ipynb file: [tip_calculator.ipynb](python/tip_calculator.ipynb "A1 Jupyter Notebook")
* git commands w/short descriptions

> #### Git commands w/short descriptions:

1. git init - creates a new repo
2. git status - displays the state of the working directory and the staging area. 
3. git add - adds a change in the working directory to the staging area
4. git commit - commits the staged snapshot to the project history
5. git push - publish new local commits on a remote server
6. git pull - used to fetch and download content from a remote repo and immediately update the local repo to match that content.
7. git merge - merge a different branch into your active branch

#### Assignment Screenshots:

#### Screenshot of *a1_tip_calculator* application running (IDLE):

![Python Installation Screenshot IDLE](img/a1_tip_calculator_idle.png "A1 IDLE Screenshot")


#### Screenshot of *a1_tip_calculator* application running (Visual Studio Code):
![Python Installation Screenshot VS Code](img/a1_tip_calculator_vs_code.png "A1 VS Code Screenshot")



#### A1 Jupyter Notebook:

![tip_calculator.ipynb](img/a1_jupyter_notebook.png "A1 Jupyter Notebook")





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bmh18v/bitbucketstationlocations/ "Bitbucket Station Locations")


