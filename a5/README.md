> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Bryan Humphries

### Assignment 5 Requirements:

#### README.md file should include the following items:

* Screenshots of code output running from the "R" tutorial and "R" assignemt code
* Screenshots of at lease two plots from the tutorial and the assignment
* Complete Python Skill sets 13, 14 and 15.

#### Assignment Screenshots:

#### Code Output From "R" tutorial and "R" assignment 

| Tutorial Output Code | Main Assignment A5 Output Code
| :-|:-
![Tutorial Output](img/learning_r_code_output.png "Learning R Output") | ![R Main Assignment Code](img/lis_4369_a5_code_output.png "R Main Assignment Output")


#### "R" Tutorial Plots

| Graph Plot 1 | Graph Plot 2
| :-|:-|:-
![Plot 1](img/plot1_learning_r.png "Tutorial Plot 1") | ![Plot 2](img/plot2_learning_r.png "Tutorial Plot 2") 

#### "R" A5 Assignment Plots

| Assignment Graph Plot 1 | Assignment Graph Plot 2 
| :-|:-|:-
![Graph Plot 1](img/plot_1_lis4369_a5.png "A5 Plot 1") | ![Graph Plot 2](img/plot_2_lis4369_a5.png "A5 Plot 2")

#### Skill Sets 13-15

| Skillset 13: Sphere Volume Calculator | Skillset 14: Calculator Error Handling Runthrough 1 
| :-|:-|:-
![Skill Set 13](img/ss13_volume_calculator.png "Sphere Volume") | ![Skill Set 14](img/ss14_part1_ss.png "Calculator Part 1") 

| Skillset 14: Calculator Error Handling Runthrough 2 | Skillset 15: File Write Read
| :-|:-
![Skill Set 14](img/ss14_part2_ss.png "Calculator Part 2") | ![Skill Set 15](img/ss15_read_write_file.png "Calculator Part 2")





