> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Bryan Humphries

### Assignment 4 Requirements:

#### README.md file should include the following items:

* Data Analysis 2 Application Running in Terminal
* Link to a4_data_analysis_2.ipynb file: [data_analysis_2.ipynb](data_analysis_2.ipynb "A4 Jupyter Notebook")
* Data Analysis 2 in Jupyter Notebook
* Screenshots of Skill Sets 10, 11 and 12 

#### Assignment Screenshots:

#### Data Analysis 2 in Terminal

| Data Analysis 2 Output in Terminal | Graph from Data Analysis 2
| :-|:-|:-
![Data Analysis 2 Terminal](img/data_analysis_2_terminal.png "Data Analysis 2 Code") | ![Screenshot of Data Analysis 2 Graph](img/data_analysis_graph.png "Data Analysis 2 Line Graph")


#### Data Analysis 2 in Jupyter Notebook

| Data Analysis Code Part 1 | Data Analysis Code Part 2 | Data Analysis Code Part 3
| :-|:-|:-
![Jupyter Notebook Code Part 1](img/jupyter_ss_1.png "JN Code Part 1") | ![Jupyter Notebook Code Part 2](img/jupyter_ss_2.png "JN Code Part 2") | ![Jupyter Notebook Code Part 3](img/jupyter_ss_3.png "JN Code Part 3")

#### Data Analysis 2 Output

| Data Analysis Output Part 1 | Data Analysis Output Part 2 | Data Analysis Output Part 3
| :-|:-|:-
![Jupyter Notebook Output Part 1](img/jupyter_output_ss1.png "JN Output Part 1") | ![Jupyter Notebook Output Part 2](img/jupyter_output_ss2.png "JN Output Part 2") | ![Jupyter Notebook Output Part 3](img/jupyter_output_ss3.png "JN Output Part 3")

#### Skill Sets 10-12

| Skillset 10: Using Dictionaries | Skillset 11: Random Number Generator | Skillset 12: Temperature Conversion Program
| :-|:-|:-
![Skill Set 10](img/ss10_screenshot.png "Using Dictionaries") | ![Skill Set 11](img/ss11_screenshot.png "Number Generator") | ![Skill Set 12](img/ss12_screenshot.png "Temperature Conversion")




